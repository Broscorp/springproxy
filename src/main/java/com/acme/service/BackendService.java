package com.acme.service;


import com.acme.controller.BadCredentialsException;
import com.acme.model.InputItems;
import java.util.Optional;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;


@Service
@Slf4j
public class BackendService {

  @Value("${backend.export.endpoint}")
  private String backendExportUrl;

  @Value("${backend.login.endpoint}")
  private String backendLoginUrl;

  @Value("${backend.companyId}")
  private String companyId;
  @Value("${backend.userName}")
  private String userName;
  @Value("${backend.password}")
  private String password;


  private final RestTemplate restTemplate;

  public BackendService(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  private Optional<String> login() {
    MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
    map.add("companyId", companyId);
    map.add("userName", userName);
    map.add("password", password);
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);
    HttpEntity<String> response
        = restTemplate.exchange(backendLoginUrl, HttpMethod.POST, entity, String.class);

    return Optional.ofNullable(response.getHeaders().getFirst("Set-Cookie"));
  }

  private InputItems fetchData(String cookie, String id, Integer count) {
    HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
    headers.add("Cookie", cookie);
    HttpEntity entity = new HttpEntity(headers);
    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(backendExportUrl).queryParam("id", id);
    if (count != null) {
      builder.queryParam("count", count);
    }
    ResponseEntity<InputItems> response
        = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, InputItems.class);
    return response.getBody();

  }

  public BackendResponse proxyRequest(String id, Integer count) {
    //noinspection Convert2MethodRef
    return login().map(cookie -> new BackendResponse(fetchData(cookie, id, count), cookie))
        .orElseThrow(() -> new BadCredentialsException());
  }

  @Data
  public static class BackendResponse {

    private final InputItems inputItems;
    private final String cookie;
  }


}
