package com.acme.model;

import io.github.threetenjaxb.core.ZonedDateTimeXmlAdapter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.ZonedDateTime;
import java.util.List;


@NoArgsConstructor
@AllArgsConstructor
@Getter
public class OutputItems {


    @XmlElement(name = "entry")
    public List<OutputItem> items;

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    public static class OutputItem {

        @XmlAttribute
        public String uuid;
        public String text;
        @XmlJavaTypeAdapter(ZonedDateTimeXmlAdapter.class)
        public ZonedDateTime dateTime;
        public Long number;
        public String author;
        @XmlElementWrapper(name = "approvers")
        @XmlElement(name = "approver")
        public List<String> approvers;

    }
}
