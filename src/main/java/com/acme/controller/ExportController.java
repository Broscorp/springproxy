package com.acme.controller;


import com.acme.model.OutputModel;
import com.acme.service.BackendService;
import com.acme.service.BackendService.BackendResponse;
import com.acme.service.TransformService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class ExportController {

    private final BackendService backendService;
    private final TransformService transformService;

    public ExportController(BackendService backendService, TransformService transformService) {
        this.backendService = backendService;
        this.transformService = transformService;
    }

    @GetMapping(value = "/export",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_XML_VALUE})
    public ResponseEntity<OutputModel> export(@RequestParam(value = "id") Integer id,
                                              @RequestParam(value = "count", required = false) Integer count) {
        log.info("Accessing /export endpoint with count = {} and id = {}", count, id);
        BackendResponse backendResponse = backendService.proxyRequest(String.valueOf(id), count);
        OutputModel outputModel = transformService.transformToOutput(backendResponse.getInputItems());

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Cookie", backendResponse.getCookie());
        return ResponseEntity.ok().headers(responseHeaders)
                .body(outputModel);

    }

}
