package com.acme.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
public class InputItems {

    private List<InputItem> items;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class InputItem {

        private String uuid;
        private String text;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
        private LocalDateTime datetime;
        private Long number;
        private String name;
        private List<String> names;

        public boolean isEmpty() {
            return uuid == null;
        }
    }

}


