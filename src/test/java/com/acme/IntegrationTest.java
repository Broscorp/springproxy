package com.acme;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

import com.acme.model.InputItems;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URL;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;


@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class IntegrationTest {

  @LocalServerPort
  private int port;

  @Autowired
  private TestRestTemplate testRestTemplate;

  @Autowired
  private ObjectMapper objectMapper;

  @MockBean
  private RestTemplate restTemplate;

  @Test
  void testJson() throws Exception {
    //setup
    MultiValueMap<String, String> cookieMap = new LinkedMultiValueMap<>();
    cookieMap.add("Set-Cookie", "test");
    ResponseEntity responseEntityLogin = new ResponseEntity(cookieMap, HttpStatus.OK);
    ResponseEntity<InputItems> responseEntityData = new ResponseEntity<>(
        objectMapper.readValue(correctInputJson, InputItems.class), HttpStatus.OK);
    doReturn(responseEntityLogin).doReturn(responseEntityData).when(restTemplate)
        .exchange(ArgumentMatchers.anyString(),
            ArgumentMatchers.any(HttpMethod.class),
            ArgumentMatchers.any(),
            ArgumentMatchers.<Class<String>>any());

    MultiValueMap<String, String> requestHeadersMap = new LinkedMultiValueMap<>();
    requestHeadersMap.add("Accept", "application/json");
    HttpEntity httpEntity = new HttpEntity(requestHeadersMap);

    //execute
    ResponseEntity<String> response = testRestTemplate.exchange(
        new URL("http://localhost:" + port + "/export?id=1&count=5").toString(), HttpMethod.GET,
        httpEntity, String.class);

    //verify
    assertEquals(outputResultJson, response.getBody());
  }

  @Test
  void testXml() throws Exception {
    //setup
    MultiValueMap<String, String> cookieMap = new LinkedMultiValueMap<>();
    cookieMap.add("Set-Cookie", "test");
    ResponseEntity responseEntityLogin = new ResponseEntity(cookieMap, HttpStatus.OK);
    ResponseEntity<InputItems> responseEntityData = new ResponseEntity<>(
        objectMapper.readValue(correctInputJson, InputItems.class), HttpStatus.OK);
    doReturn(responseEntityLogin).doReturn(responseEntityData).when(restTemplate)
        .exchange(ArgumentMatchers.anyString(),
            ArgumentMatchers.any(HttpMethod.class),
            ArgumentMatchers.any(),
            ArgumentMatchers.<Class<String>>any());

    MultiValueMap<String, String> requestHeadersMap = new LinkedMultiValueMap<>();
    requestHeadersMap.add("Accept", "text/xml");
    HttpEntity httpEntity = new HttpEntity(requestHeadersMap);

    //execute
    ResponseEntity<String> response = testRestTemplate.exchange(
        new URL("http://localhost:" + port + "/export?id=1&count=5").toString(), HttpMethod.GET,
        httpEntity, String.class);

    //verify
    assertEquals(outputResultXml, response.getBody());
  }


  @Test
  void testJsonWithoutCount() throws Exception {
    //setup
    MultiValueMap<String, String> cookieMap = new LinkedMultiValueMap<>();
    cookieMap.add("Set-Cookie", "test");
    ResponseEntity responseEntityLogin = new ResponseEntity(cookieMap, HttpStatus.OK);
    ResponseEntity<InputItems> responseEntityData = new ResponseEntity<>(
        objectMapper.readValue(correctInputJson, InputItems.class), HttpStatus.OK);
    doReturn(responseEntityLogin).doReturn(responseEntityData).when(restTemplate)
        .exchange(ArgumentMatchers.anyString(),
            ArgumentMatchers.any(HttpMethod.class),
            ArgumentMatchers.any(),
            ArgumentMatchers.<Class<String>>any());

    MultiValueMap<String, String> requestHeadersMap = new LinkedMultiValueMap<>();
    requestHeadersMap.add("Accept", "application/json");
    HttpEntity httpEntity = new HttpEntity(requestHeadersMap);

    //execute
    ResponseEntity<String> response = testRestTemplate.exchange(
        new URL("http://localhost:" + port + "/export?id=1").toString(), HttpMethod.GET, httpEntity,
        String.class);

    //verify
    assertEquals(outputResultJson, response.getBody());
  }


  @Test
  void testJsonEmptyResponse() throws Exception {
    //setup
    MultiValueMap<String, String> cookieMap = new LinkedMultiValueMap<>();
    cookieMap.add("Set-Cookie", "test");
    ResponseEntity responseEntityLogin = new ResponseEntity(cookieMap, HttpStatus.OK);
    ResponseEntity<InputItems> responseEntityData = new ResponseEntity<>(
        objectMapper.readValue("{\"items\":[]}",
            InputItems.class), HttpStatus.OK);
    doReturn(responseEntityLogin).doReturn(responseEntityData).when(restTemplate)
        .exchange(ArgumentMatchers.anyString(),
            ArgumentMatchers.any(HttpMethod.class),
            ArgumentMatchers.any(),
            ArgumentMatchers.<Class<String>>any());

    MultiValueMap<String, String> requestHeadersMap = new LinkedMultiValueMap<>();
    requestHeadersMap.add("Accept", "application/json");
    HttpEntity httpEntity = new HttpEntity(requestHeadersMap);
    String emptyOutputJson = "{\"meta\":{\"count\":0},\"data\":" +
        "{\"items\":[]}}";

    //execute
    ResponseEntity<String> response = testRestTemplate.exchange(
        new URL("http://localhost:" + port + "/export?id=1").toString(), HttpMethod.GET, httpEntity,
        String.class);

    //verify
    assertEquals(emptyOutputJson, response.getBody());
  }


  private final String correctInputJson = "{\n"
      + "\"items\":[{\n"
      + "\"uuid\":\"82EA85C3B0389980C12583DA00490371\",\n"
      + "\"text\":\"Some text...\",\n"
      + "\"datetime\":\"12-04-2019 15:17:34\",\n"
      + "\"number\":589318684672313,\n"
      + "\"name\":\"CN=John Doe/O=ACME\",\n"
      + "\"names\":[\n"
      + "\"CN=John Doe/O=ACME\",\n"
      + "\"CN=John Doe/O=ACME\",\n"
      + "\"CN=John Doe/O=ACME\"\n"
      + "]\n"
      + "},{}\n"
      + "]}";

  private final String outputResultJson = "{\"meta\":{\"count\":1},\"data\":" +
      "{\"items\":[{\"uuid\":\"82EA85C3B0389980C12583DA00490371\"," +
      "\"text\":\"Some text...\",\"dateTime\":\"2019-04-12T13:17:34Z\"," +
      "\"number\":589318684672313,\"author\":\"CN=John Doe/O=ACME\",\"approvers\"" +
      ":[\"CN=John Doe/O=ACME\",\"CN=John Doe/O=ACME\",\"CN=John Doe/O=ACME\"]}]}}";


  private final String outputResultXml =
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?" +
          "><export><meta><count>1</count></meta><entries><entry uuid=\"82EA85C3B0389980C12583DA00490371\">"
          +
          "<text>Some text...</text><dateTime>2019-04-12T13:17:34Z</dateTime><number>589318684672313</number>"
          +
          "<author>CN=John Doe/O=ACME</author><approvers><approver>CN=John Doe/O=ACME</approver>" +
          "<approver>CN=John Doe/O=ACME</approver><approver>CN=John Doe/O=ACME</approver></approvers>"
          +
          "</entry></entries></export>";

}
