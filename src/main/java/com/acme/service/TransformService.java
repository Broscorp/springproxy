package com.acme.service;

import com.acme.model.InputItems;
import com.acme.model.OutputItems;
import com.acme.model.OutputModel;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class TransformService {


  public OutputModel transformToOutput(InputItems items) {
    OutputModel outputModel = new OutputModel();
    List<OutputItems.OutputItem> outputItemList = items.getItems().stream()
        .filter(inputItem -> !inputItem.isEmpty())
        .map(inputItem -> new OutputItems.OutputItem(inputItem.getUuid(), inputItem.getText(),
            cetToUtc(inputItem.getDatetime())
            , inputItem.getNumber(), inputItem.getName(),
            inputItem.getNames())).collect(Collectors.toList());
    outputModel.meta = new OutputModel.Meta(outputItemList.size());
    outputModel.data = new OutputItems(outputItemList);
    return outputModel;
  }


  private ZonedDateTime cetToUtc(LocalDateTime timeInCet) {
    ZonedDateTime cetTimeZoned = ZonedDateTime.of(timeInCet, ZoneId.of("CET"));
    return cetTimeZoned.withZoneSameInstant(ZoneOffset.UTC);
  }

}
