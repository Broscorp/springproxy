package com.acme.model;

import io.swagger.annotations.SwaggerDefinition;
import lombok.Data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;

@SwaggerDefinition(
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"}
)
@XmlRootElement(name = "export")
@Getter
public class OutputModel {

    @XmlElement
    public Meta meta;
    @XmlElement(name = "entries")
    public OutputItems data;


    @Data
    public static class Meta {

        @XmlElement
        private final Integer count;
    }

}
