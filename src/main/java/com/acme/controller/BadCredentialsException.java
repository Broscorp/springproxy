package com.acme.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class BadCredentialsException extends RuntimeException {

    public BadCredentialsException() {
        super("Couldn't login to remote backend");
    }
}
