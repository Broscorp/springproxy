package com.acme.controller;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.acme.model.InputItems;
import com.acme.model.OutputModel;
import com.acme.service.BackendService;
import com.acme.service.TransformService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class ExportControllerTest {


  @Autowired
  private ExportController exportController;

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private BackendService backendService;
  @MockBean
  private TransformService transformService;


  @BeforeEach
  void setup() {
    OutputModel outputModel = new OutputModel();
    InputItems inputItems = new InputItems();
    BackendService.BackendResponse backendResponse = new BackendService.BackendResponse(inputItems,
        "cookie");
    when(backendService.proxyRequest(anyString(), any())).thenReturn(backendResponse);
    when(transformService.transformToOutput(any())).thenReturn(outputModel);
  }

  @Test
  void contexLoads() {
    assertThat(exportController).isNotNull();
  }

  @Test
  void testExportWithoutCount() throws Exception {
    //execute and verify
    mockMvc.perform(get("/export?id=1")
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .content(""))
        .andExpect(status().isOk())
        .andReturn();
  }

  @Test
  void testExportWithCount() throws Exception {
    //verify & execute
    mockMvc.perform(get("/export?id=1&count=5")
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .content(""))
        .andExpect(status().isOk())
        .andReturn();
  }

  @Test
  void testExportWrongMedia() throws Exception {
    //execute & verify
    mockMvc.perform(get("/export?id=1&count=4")
        .accept(MediaType.APPLICATION_PDF)
        .contentType(MediaType.APPLICATION_PDF)
        .content(""))
        .andExpect(status().is4xxClientError())
        .andReturn();
  }

}
