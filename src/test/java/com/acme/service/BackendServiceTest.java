package com.acme.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;

import com.acme.controller.BadCredentialsException;
import com.acme.model.InputItems;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
class BackendServiceTest {

  @Autowired
  private BackendService backendService;

  @Autowired
  private ObjectMapper objectMapper;

  @MockBean
  private RestTemplate restTemplate;

  @Test
  void testLoginOk() throws Exception {
    //setup
    MultiValueMap<String, String> cookieMap = new LinkedMultiValueMap<>();
    cookieMap.add("Set-Cookie", "test");
    ResponseEntity responseEntityLogin = new ResponseEntity(cookieMap, HttpStatus.OK);
    ResponseEntity<InputItems> responseEntityData = new ResponseEntity<>(
        objectMapper.readValue("{}", InputItems.class), HttpStatus.OK);
    doReturn(responseEntityLogin).doReturn(responseEntityData).when(restTemplate)
        .exchange(ArgumentMatchers.anyString(),
            ArgumentMatchers.any(HttpMethod.class),
            ArgumentMatchers.any(),
            ArgumentMatchers.<Class<String>>any());
    //execute
    BackendService.BackendResponse response = backendService.proxyRequest("1", 2);
    //verify
    assertNotNull(response.getCookie());
    assertNotNull(response.getInputItems());
  }

  @Test
  void testLoginFailed() throws Exception {
    //setup
    MultiValueMap<String, String> emptyCookieMap = new LinkedMultiValueMap<>();
    ResponseEntity responseEntityLogin = new ResponseEntity(emptyCookieMap, HttpStatus.OK);
    ResponseEntity<InputItems> responseEntityData = new ResponseEntity<>(
        objectMapper.readValue("{}", InputItems.class), HttpStatus.OK);
    doReturn(responseEntityLogin).doReturn(responseEntityData).when(restTemplate)
        .exchange(ArgumentMatchers.anyString(),
            ArgumentMatchers.any(HttpMethod.class),
            ArgumentMatchers.any(),
            ArgumentMatchers.<Class<String>>any());
    //execute
    assertThrows(BadCredentialsException.class,
        () -> backendService.proxyRequest("1", 2),
        "Expected doThing() to throw, but it didn't");
  }


}
