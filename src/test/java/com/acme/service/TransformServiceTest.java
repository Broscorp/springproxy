package com.acme.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.acme.model.InputItems;
import com.acme.model.OutputModel;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;

class TransformServiceTest {

  private final TransformService transformService = new TransformService();

  @Test
  void checkDateConversion() {
    //setup
    List<InputItems.InputItem> items = new ArrayList<>();
    InputItems.InputItem item = new InputItems.InputItem("123",
        "some text", LocalDateTime.of(2019, 10, 4, 5, 30, 30),
        1L, "somename",
        Stream.of("1", "2").collect(Collectors.toList()));
    items.add(item);
    InputItems inputItems = new InputItems();
    inputItems.setItems(items);
    //execute
    OutputModel ot = transformService.transformToOutput(inputItems);
    //verify
    assertNotNull(ot);
    assertNotNull(ot.data);
    assertEquals(ot.data.items.get(0).dateTime.toLocalDateTime(),
        item.getDatetime().minusHours(2));
  }

  @Test
  void checkFilterEmptyItem() {
    //setup
    List<InputItems.InputItem> items = new ArrayList<>();
    InputItems.InputItem item = new InputItems.InputItem("123",
        "some text", LocalDateTime.now(),
        1L, "somename",
        Stream.of("1", "2").collect(Collectors.toList()));
    items.add(item);
    items.add(new InputItems.InputItem());
    InputItems inputItems = new InputItems();
    inputItems.setItems(items);
    //execute
    OutputModel ot = transformService.transformToOutput(inputItems);
    //verify
    assertNotNull(ot);
    assertNotNull(ot.data);
    assertEquals(ot.data.items.size() + 1, items.size());
  }
}
